// -*- C++ -*-
//
// Package:    DarkPhotonsAnalysis/DarkPhotonAnalyzer
// Class:      DarkPhotonAnalyzer
//
/**\class DarkPhotonAnalyzer DarkPhotonAnalyzer.cc DarkPhotonsAnalysis/DarkPhotonAnalyzer/plugins/DarkPhotonAnalyzer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Alexander Savin
//         Created:  Thu, 21 Jan 2021 21:02:34 GMT
//
//

// system include files
#include <memory>
#include <string>
#include <iostream>
#include <vector>
#include <set>
// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "FWCore/Common/interface/TriggerNames.h"
#include "FWCore/Common/interface/TriggerResultsByName.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"

#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticleFwd.h"

#include "TTree.h"
#include "TriggerCode/DPIO/interface/Muon.h"
#include "TriggerCode/DPIO/interface/Event.h"
#include "TriggerCode/DPIO/interface/GenMuonSelector.h"
#include "TriggerCode/DPIO/interface/FinalGenMuonSelector.h"

//
// class declaration
//


// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.


class DarkPhotonAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources> {
public:
  explicit DarkPhotonAnalyzer(const edm::ParameterSet&);
  ~DarkPhotonAnalyzer();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  
private:
  void beginJob() override;
  void analyze(const edm::Event&, const edm::EventSetup&) override;
  void endJob() override;

  // ----------member data ---------------------------

  bool debug_;
  ULong64_t indexevents_;
  Int_t       runNumber_;
  Int_t            lumi_;
  

  edm::EDGetTokenT< edm::TriggerResults >                    t_myTriggerResults_;
  edm::EDGetTokenT< trigger::TriggerEvent >                  t_myTriggerEvent_;

  edm::EDGetTokenT< reco::GenParticleCollection >            t_genParticle_;

  TTree* eventTree;
  DPIO::Event eventStore{std::vector<DPIO::Muon>{}, std::set<std::string>{}};
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
DarkPhotonAnalyzer::DarkPhotonAnalyzer(const edm::ParameterSet& iConfig)
    : debug_  (iConfig.getUntrackedParameter<bool>("debug",false)),
      t_myTriggerResults_  ( consumes< edm::TriggerResults >     (iConfig.getUntrackedParameter<edm::InputTag>("myTriggerResults"  )) ),
      t_myTriggerEvent_    ( consumes< trigger::TriggerEvent >   (iConfig.getUntrackedParameter<edm::InputTag>("myTriggerEvent"    )) ),
      t_genParticle_       ( consumes< reco::GenParticleCollection >   (iConfig.getUntrackedParameter<edm::InputTag>("genParticle"       )) )
{
  //now do what ever initialization is needed
} 

DarkPhotonAnalyzer::~DarkPhotonAnalyzer() {
  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//




// ------------ method called for each event  ------------
void DarkPhotonAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {

  using namespace edm;

  indexevents_ = iEvent.id().event();
  runNumber_ = iEvent.id().run();
  lumi_ = iEvent.luminosityBlock();
 
  if (debug_) std::cout << " Event: " << indexevents_ << " run: " << runNumber_ << " lumi: " << lumi_ << std::endl ;

  edm::Handle<edm::TriggerResults>  h_triggerResults;
  edm::Handle<trigger::TriggerEvent> h_triggerEvent;

  iEvent.getByToken(t_myTriggerResults_, h_triggerResults);
  iEvent.getByToken(t_myTriggerEvent_,   h_triggerEvent);


  edm::Handle<reco::GenParticleCollection> h_genParticle;
  iEvent.getByToken(t_genParticle_, h_genParticle);

  edm::TriggerNames triggerNames = iEvent.triggerNames(*h_triggerResults);
  
  std::set<std::string> acceptedTriggerNames;

  
  for(size_t i = 0; i < triggerNames.size(); ++i)
  {
      if(h_triggerResults->accept(i))
      {
        //std::cerr << "Trigger Accepted: " << triggerNames.triggerName(i) << '\n';
        acceptedTriggerNames.insert(triggerNames.triggerName(i));
      }
  }

  std::vector<DPIO::Muon> muons = DPIO::Muon::makeFromMuonSelector(DPIO::FinalGenMuonSelector(h_genParticle));
  eventStore = DPIO::Event(muons, acceptedTriggerNames);

  eventTree->Fill();
}

// ------------ method called once each job just before starting event loop  ------------
void DarkPhotonAnalyzer::beginJob() {
  // please remove this method if not needed
  edm::Service<TFileService> fs;
  eventTree = fs->make<TTree>("EventList","Event List");
  eventTree->Branch("eventNumber",&indexevents_);
  eventTree->Branch("event",&eventStore);
}

// ------------ method called once each job just after ending the event loop  ------------
void DarkPhotonAnalyzer::endJob() {
  // please remove this method if not needed
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void DarkPhotonAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  //Specify that only 'tracks' is allowed
  //To use, remove the default given above and uncomment below
  //ParameterSetDescription desc;
  //desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  //descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(DarkPhotonAnalyzer);
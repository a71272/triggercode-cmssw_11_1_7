import FWCore.ParameterSet.Config as cms

DarkPhotonAnalyzer = cms.EDAnalyzer("DarkPhotonAnalyzer",
                                myTriggerResults   = cms.untracked.InputTag("TriggerResults",       "", "MYHLT"),
                           	myTriggerEvent     = cms.untracked.InputTag("hltTriggerSummaryAOD", "", "MYHLT"),
#                                genEventInfo = cms.untracked.InputTag("generator"),
	                        genParticle = cms.untracked.InputTag("genParticles")
)

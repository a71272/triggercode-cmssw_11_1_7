 #include "plot_tdr_lib.cc"

//Definition of drawing efficiency
//File name is the base file name (no extension) that the histogram will be saved to
//If deleteCanvas is true, the canvas is deleted at the end of the run.
void plot_tdr_efficiency(std::vector<TriggerEfficiency>& triggerEfficiencies, const std::string& fileName, 
                          const GraphParameters& gps, const LegendParameters& lps, const std::string& genName)
{

  //Related to the Plotting style
   gStyle->SetLineWidth(2);
   //Canvas size according to the FTR
   TCanvas* c = new TCanvas("c", "c",473,281,800,727);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetGridx();
   c->SetGridy();
   c->SetTickx(1);
   c->SetTicky(1);
   c->SetLeftMargin(0.1278196);
   c->SetRightMargin(0.03884712);
   c->SetBottomMargin(0.1232092);
   c->SetFrameLineWidth(2);
   c->SetFrameBorderMode(0);
   c->SetFrameLineWidth(2);
   c->SetFrameBorderMode(0);
  

   //Adding in and setting up the multigraph
   TMultiGraph* multigraph = new TMultiGraph();
   multigraph->GetXaxis()->SetTitle((genName+" gen p^{#mu}_{T} [GeV]").data());
	 multigraph->GetYaxis()->SetTitle("Efficiency/Event ");
   multigraph->GetXaxis()->SetTitleSize(0.045);
   multigraph->GetYaxis()->SetTitleSize(0.045);
   multigraph->GetXaxis()->SetTitleOffset(1.00);
   multigraph->GetYaxis()->SetTitleOffset(1.00);
   multigraph->GetXaxis()->SetLimits(gps.x_min,gps.x_max);
   multigraph->SetMinimum(gps.y_min);
   multigraph->SetMaximum(gps.y_max);

   //Deque to store all trigger efficiency graphs conveniently here
   //We are pushing to the front so we are using a deque
   std::deque<TGraphAsymmErrors*> triggerEfficiencyGraphs;
   //Making all trigger efficiency graphs
   //Drawing them in reverse order so the ones that go first are on top
   for(int i (triggerEfficiencies.size()-1); i >= 0; --i)
   {
     TGraphAsymmErrors* graph = triggerEfficiencies.at(i).makeGraph();
     multigraph->Add(graph);
     triggerEfficiencyGraphs.push_front(graph);
   }
   // CMS Name Label on top bar//
   TLatex* cms_top = new TLatex(0.55,0.91,"#bf{CMS Phase-2}                                            PU200 (14 TeV)");
   cms_top->SetNDC();
   cms_top->SetNDC();
   cms_top->SetTextFont(42);
   cms_top->SetTextSize(0.04);
   cms_top->SetTextAlign(20);
   
   // CMS plotting type definiation
   TLatex* cms_type = new TLatex(0.16,0.84,"#it{Simulation}");
   cms_type->SetNDC();
   cms_type->SetTextFont(42);
   cms_type->SetTextSize(0.04);
   cms_type->SetTextColor(kBlack);


   //Legend definiation//
   TLegend* legend_cms = new TLegend(lps.x_min, lps.y_min, lps.x_max, lps.y_max);
   for(size_t i = 0; i < triggerEfficiencyGraphs.size(); ++i)
   {
     TLegendEntry* entry = legend_cms->AddEntry(triggerEfficiencyGraphs[i], triggerEfficiencies[i].getLegendName().data(), "lp");
     entry->SetTextColor(triggerEfficiencies[i].getStyleParams().color);
   }
   legend_cms->SetEntrySeparation(0.5);
   legend_cms->SetFillStyle(0);
   legend_cms->SetTextFont(42);
   //Cannot change separation between the entries so the text has to be small.
   //Edit: CHANGE THE SIZE
   legend_cms->SetTextSize(0.03);
   legend_cms->SetFillColor(0);
   legend_cms->SetBorderSize(0);
   

   // Process name : HH --> ggbb
   TLatex* text_proc = new TLatex(0.16,0.78,"#font[61]{pp#rightarrow#tilde{q}#bar{#tilde{q}}#rightarrow#tilde{#chi}#bar{#tilde{#chi}}"
                                            "#rightarrow#gamma_{d}#gamma_{d}#rightarrow#mu^{+}#mu^{-}#mu^{+}#mu^{-}}");
   text_proc->SetNDC();
   text_proc->SetTextFont(42);
   text_proc->SetTextSize(0.04);
   text_proc->SetTextColor(kBlack);

   //Drawing all of the things
   multigraph->Draw("AP");
   legend_cms->Draw("same");
   cms_top->Draw("same");
   cms_type->Draw("same");
   text_proc->Draw("same");

  
   c->SetFrameLineWidth(2);
   c->SetFrameBorderMode(0);
   c->Update();

  //Save the canvas in formats specified
  for(const std::string& extension : gps.saveExtensions)
  {
    std::string fullName = fileName + extension;
    if(gps.noSpaces)
    {
      std::string::iterator end_pos = std::remove(fullName.begin(),fullName.end(),' ');
      fullName.erase(end_pos,fullName.end());
    }
    c->Print(fullName.c_str());
  }
  if(gps.deleteCanvas) delete c;
   
}


struct TDRFilters : public TriggerEfficiencyInitializer::TriggerFilters
{
  TDRFilters (double i_maxEta, double i_minPt, int i_motherPdgId, int i_minMuons) : maxEta(i_maxEta), minPt(i_minPt), motherPdgId(i_motherPdgId), minMuons(i_minMuons) {}
    std::vector<DPIO::Muon> muonFilter(const DPIO::Event& event) override
    {
        std::vector<DPIO::Muon> filteredMuons;
        std::set<int> motherPdgIds;
        for(const DPIO::Muon& muon : event.getMuons())
        {
            for(int pdgId : muon.getMotherPdgIds()) motherPdgIds.emplace(std::abs(pdgId));
            if((abs(muon.getEta()) < maxEta) && (muon.getPt() > minPt) && (motherPdgIds.find(std::abs(motherPdgId)) != motherPdgIds.end()))
                filteredMuons.emplace_back(muon);
            motherPdgIds.clear();
        }
        return filteredMuons;
    }
    bool eventFilter(unsigned long long eventNum, const DPIO::Event& event, const std::vector<DPIO::Muon>& filteredMuons) override
    {
        return filteredMuons.size() >= minMuons;
    }
    double maxEta;
    double minPt;
    int motherPdgId;
    int minMuons;
};

//Main function
void plot_tdr()
{
  ROOT::EnableImplicitMT(); 

  using StyleParams = TriggerEfficiency::StyleParams;
  using FitParams = TriggerEfficiency::FitParams;
  using TriggerPair = std::pair<std::string,std::vector<std::string>>;
  using TriggerFilters = TriggerEfficiencyInitializer::TriggerFilters;
  //All constants
  const std::vector<TriggerPair> levelOneTriggers = {
            {"Level One All (OR)", {"L1_SingleTkMuon_22","L1_DoubleTkMuon_17_8","L1_TripleTkMuon_5_3_3"}},
            {"L1_SingleTkMuon_22", {"L1_SingleTkMuon_22"}},
            {"L1_DoubleTkMuon_17_8", {"L1_DoubleTkMuon_17_8"}},
            {"L1_TripleTkMuon_5_3_3", {"L1_TripleTkMuon_5_3_3"}}};

  const std::vector<TriggerPair> highLevelTriggers = {
            {"High Level All (OR)", {"HLT_Mu50_FromL1TkMuon","HLT_Mu37_Mu27_FromL1TkMuon","HLT_TriMu_10_5_5_DZ_FromL1TkMuon"}},
            {"HLT_Mu50_FromL1TkMuon", {"HLT_Mu50_FromL1TkMuon"}},
            {"#splitline{HLT_Mu37_Mu27_}{FromL1TkMuon}", {"HLT_Mu37_Mu27_FromL1TkMuon"}},
            {"#splitline{HLT_TriMu_10_5_5_DZ}{_FromL1TkMuon}", {"HLT_TriMu_10_5_5_DZ_FromL1TkMuon"}}};
  
  const std::vector<StyleParams> efficiencyStyles {StyleParams{kBlack, 20},StyleParams{kRed, 21},StyleParams{kBlue, 22},StyleParams{kMagenta, 23}};
  const std::vector<std::string> ascendingStrings {"Third highest", "Second highest", "Highest"};
  const LegendParameters levelOne {0.56, 0.16, 0.90, 0.27}, highLevel {0.535, 0.14, 0.90, 0.44};
  std::shared_ptr<TriggerFilters> filters (new TDRFilters(2.1,5,4900022,4));
  const std::vector<double> varsize_bins {5,10,15,20,25,30,35,40,50,60,70};
  const std::vector<std::string> saveExtensions {".png", ".pdf"};
  const bool counts = true;
  const ROOT::RDF::TH1DModel var_model {"","",int(varsize_bins.size()-1),varsize_bins.data()}, model {"","",19,5,100};
  const GraphParameters params{0,100,0,1.3,false,false,saveExtensions}, var_params{0,70,0,1.3,false,false,saveExtensions};

  TChain* chain = new TChain();
  chain->AddFile("combined.root",TTree::kMaxEntries,"DarkPhotonAnalyzer/EventList");
  TriggerEfficiencyInitializer triggerEfficiencyInitializer (*chain, filters, var_model);

  std::vector<TriggerEfficiency> triggerEfficiencies;
  
  //Level One Triggers
  //Adding Triggers
  for(const TriggerPair& triggerPair : levelOneTriggers)
  {
    triggerEfficiencyInitializer.addTrigger(triggerPair.first,triggerPair.second,false);
    if(counts)
      std::cerr << triggerPair.first << " count: " << triggerEfficiencyInitializer.getCount(triggerPair.first) << '\n';
  }
  
  for(int i = 0; i < 3; ++i)
  {
    for(int j = 0; j < 4; ++j)
      triggerEfficiencies.emplace_back(triggerEfficiencyInitializer.getEfficiency(levelOneTriggers[j].first,2-i,efficiencyStyles[j]));
    plot_tdr_efficiency(triggerEfficiencies,ascendingStrings[i]+" level one trigger efficiencies",var_params,levelOne,ascendingStrings[i]);
    triggerEfficiencies.clear();
  }

  //High level Triggers
  //Adding Triggers
  for(const TriggerPair& triggerPair : highLevelTriggers)
  {
    triggerEfficiencyInitializer.addTrigger(triggerPair.first,triggerPair.second,false);
    if(counts)
      std::cerr << triggerPair.first << " count: " << triggerEfficiencyInitializer.getCount(triggerPair.first) << '\n';
  }

  for(int i = 0; i < 3; ++i)
  {
    for(int j = 0; j < 4; ++j)
      triggerEfficiencies.emplace_back(triggerEfficiencyInitializer.getEfficiency(highLevelTriggers[j].first,2-i,efficiencyStyles[j]));
    plot_tdr_efficiency(triggerEfficiencies,ascendingStrings[i]+" high level trigger efficiencies",var_params,highLevel,ascendingStrings[i]);
    triggerEfficiencies.clear();
  }

  if(counts)
    {
      //Finding the counts of the double triggers
      triggerEfficiencyInitializer.addTrigger("L1SingleDouble",{"L1_SingleTkMuon_22","L1_DoubleTkMuon_17_8"},false);
      std::cerr << "L1SingleDouble count: " << triggerEfficiencyInitializer.getCount("L1SingleDouble") << '\n';
      triggerEfficiencyInitializer.addTrigger("HighLevelSingleDouble",{"HLT_Mu50_FromL1TkMuon","HLT_Mu37_Mu27_FromL1TkMuon"},false);
      std::cerr << "HighLevelSingleDouble count: " << triggerEfficiencyInitializer.getCount("HighLevelSingleDouble") << '\n';
      std::cerr << "Total count: " << triggerEfficiencyInitializer.getCount() << '\n';
    }
}

#include <string>
#include <string_view>
#include <locale>
#include "TChain.h"
#include "TFile.h"
#include "TH1.h"
#include "TTree.h"
#include "TKey.h"
#include "Riostream.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TDirectory.h"
#include "TF1.h"
#include "TFile.h"
#include "TFitResult.h"
#include "TGraph.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphErrors.h"
#include "THStack.h"
#include "TLegend.h"
#include "TMinuit.h"
#include "TMath.h"
#include "TProfile.h"
#include "TPaveStats.h"
#include "TPaveText.h"
#include "TROOT.h"
#include "TString.h"
#include "TStyle.h"
#include "TGraphAsymmErrors.h"
#include <iostream>
#include <exception>
#include <iomanip>
#include <vector>
#include <deque>
#include "TString.h"
#include "TFrame.h"
#include "TSystem.h"
#include "TInterpreter.h"
#include <utility>
#include <algorithm>
#include "TMultiGraph.h"
#include "TEfficiency.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include <optional>
#include <memory>
#include "TriggerCode/DPIO/interface/Event.h"
#include "ROOT/RDataFrame.hxx"

//Class that stores information about a trigger histogram
class TriggerEfficiency
{
  public:
    struct StyleParams
    {
      EColor color;
      Int_t markerStyle;
      Int_t lineWidth = 2;
      TEfficiency::EStatOption statisticOption = TEfficiency::kFCP;
    };
    struct FitParams
    {
      std::function<Double_t(const Double_t*, const Double_t*)> function;
      Double_t min, max;
      std::vector<Double_t> parameters;
      Int_t lineWidth = 5;
      std::string fitString = "R";
      Int_t numPoints = 5000;
    };
    TriggerEfficiency(const TH1D& i_passedHistogram, const TH1D& i_totalHistogram, 
                    std::string_view i_legendName, const StyleParams& i_styleParams,
                     const std::optional<FitParams>& i_fitParams);
    TEfficiency* getTriggerEfficiency(){return triggerEfficiency;}
    const std::string& getLegendName() {return legendName;}
    const StyleParams& getStyleParams(){return styleParams;}
    TGraphAsymmErrors* makeGraph();
  private:
    TEfficiency* triggerEfficiency;
    //Could be null if not initialized with fitParams
    TF1* fitFunction;
    std::string fitString;
    //Name in the legend
    std::string legendName;
    StyleParams styleParams;
};

//Constructor
TriggerEfficiency::TriggerEfficiency(const TH1D& i_passedHistogram, const TH1D& i_totalHistogram,
                    std::string_view i_legendName, const StyleParams& i_styleParams,
                     const std::optional<FitParams>& fitParams = std::nullopt)
  : triggerEfficiency(new TEfficiency(i_passedHistogram, i_totalHistogram)), legendName(i_legendName), styleParams(i_styleParams)
{
  triggerEfficiency->SetMarkerStyle(styleParams.markerStyle);
  triggerEfficiency->SetStatisticOption(styleParams.statisticOption);
  if(fitParams)
  {
    fitFunction = new TF1((std::string{i_legendName}+"Func").data(),fitParams->function,fitParams->min,fitParams->max,static_cast<Int_t>(fitParams->parameters.size()));
    fitFunction->SetParameters(fitParams->parameters.data());
    fitFunction->SetNpx(fitParams->numPoints);
    fitFunction->SetLineWidth(fitParams->lineWidth);
    fitFunction->SetLineColor(styleParams.color);
    fitString = fitParams->fitString;
  }
  else
    fitFunction = nullptr;
}


//Get graph method
TGraphAsymmErrors* TriggerEfficiency::makeGraph()
{
  TGraphAsymmErrors* triggerEfficiencyGraph = triggerEfficiency->CreateGraph();
  triggerEfficiencyGraph->SetMarkerColor(styleParams.color);
  triggerEfficiencyGraph->SetLineWidth(styleParams.lineWidth);
	triggerEfficiencyGraph->SetLineColor(styleParams.color);
  if(fitFunction) triggerEfficiencyGraph->Fit(fitFunction,fitString.data());
  return triggerEfficiencyGraph;
}


//Class that initializes an RDataFrame and outputs histograms for muon order
class TriggerEfficiencyInitializer
{
    public:
      //Initial filters for the muons and events
      struct TriggerFilters
      {
        virtual std::vector<DPIO::Muon> muonFilter(const DPIO::Event& event) = 0;
        virtual bool eventFilter(ULong64_t eventNum, const DPIO::Event& event, const std::vector<DPIO::Muon>& filteredMuons) = 0;
        virtual ~TriggerFilters() = default;
      };

      TriggerEfficiencyInitializer(TChain& chain, std::shared_ptr<TriggerFilters> triggerFilters, const ROOT::RDF::TH1DModel& model);
      void addTrigger(const std::string& triggerName, const std::vector<std::string>& listOfTriggerSims, bool allOrOne);
      TriggerEfficiency getEfficiency(const std::string& triggerName, int rank, const TriggerEfficiency::StyleParams& styleParams, 
                                      const std::optional<TriggerEfficiency::FitParams>& fitParams);
      ULong64_t getCount();
      ULong64_t getCount(const std::string& triggerName);
    private:
      //Functor returning a muon pt with a given rank
      class getRankedMuonPt
      {
        public:
          getRankedMuonPt(int i_rank) : rank(i_rank) {}
          double operator()(const std::vector<DPIO::Muon>& filteredMuons);
        private:
          int rank;
      };
      ROOT::RDataFrame dataFrame;
      ROOT::RDF::RNode filteredDataFrame;
      ROOT::RDF::RResultPtr<ULong64_t> filteredCount;
      ROOT::RDF::TH1DModel model;
      //Stores trigger filters and results
      struct TriggerStore
      {
        ROOT::RDF::RNode filterNode;
        std::map<int,TH1D> rankHistograms;
        ROOT::RDF::RResultPtr<ULong64_t> filterCount;
        TriggerStore(const ROOT::RDF::RNode& rNode);
      };
      std::map<std::string,TriggerStore> triggerMap;
      std::map<int,TH1D> total;
};

double TriggerEfficiencyInitializer::getRankedMuonPt::operator()(const std::vector<DPIO::Muon>& filteredMuons)
{
  std::vector<double> muonPts;
  for(const DPIO::Muon& muon : filteredMuons)
    muonPts.push_back(muon.getPt());
  std::sort(muonPts.begin(), muonPts.end(), std::greater<double>());
  return muonPts[rank];
}

TriggerEfficiencyInitializer::TriggerStore::TriggerStore(const ROOT::RDF::RNode& rNode) :
  filterNode(rNode),
  filterCount(filterNode.Count())
{}

TriggerEfficiencyInitializer::TriggerEfficiencyInitializer(TChain& chain, std::shared_ptr<TriggerFilters> triggerFilters, 
                                                            const ROOT::RDF::TH1DModel& i_model) :
                                                            dataFrame(chain),
                                                            filteredDataFrame(dataFrame.Define("filteredMuons",[triggerFilters](const DPIO::Event& event)
                                                            {
                                                              return triggerFilters->muonFilter(event);
                                                            },{"event"}).Filter([triggerFilters](ULong64_t eventNum, const DPIO::Event& event, const std::vector<DPIO::Muon>& filteredMuons)
                                                            {
                                                              return triggerFilters->eventFilter(eventNum,event,filteredMuons);
                                                            },{"eventNumber","event","filteredMuons"})),
                                                            filteredCount(filteredDataFrame.Count()),
                                                            model(i_model)
{
}

void TriggerEfficiencyInitializer::addTrigger(const std::string& triggerName, const std::vector<std::string>& listOfTriggerSims, bool allOrOne)
{
  triggerMap.emplace(triggerName, TriggerStore(filteredDataFrame.Filter([listOfTriggerSims,allOrOne](const DPIO::Event& event){
    const std::set<std::string>& triggerSet = event.getAcceptedTriggers();
    for(const std::string& trigger : listOfTriggerSims)
    {
      if(allOrOne)
      {
        if(triggerSet.find(trigger) == triggerSet.end())
          return false;
      }
      else
      {
        if(triggerSet.find(trigger) != triggerSet.end())
          return true;
      }
    }
    return allOrOne;
  }, {"event"})));
}

TriggerEfficiency TriggerEfficiencyInitializer::getEfficiency(const std::string& triggerName, int rank,
                                                              const TriggerEfficiency::StyleParams& styleParams, 
                                                              const std::optional<TriggerEfficiency::FitParams>& fitParams = std::nullopt)
{
  if(triggerMap.find(triggerName) == triggerMap.end())
    throw cms::Exception(triggerName+" Efficiency not found!");
  
  std::map<int,TH1D>& passedHistograms = triggerMap.find(triggerName)->second.rankHistograms;

  if(passedHistograms.find(rank) == passedHistograms.end())
    passedHistograms[rank] = triggerMap.find(triggerName)->second.filterNode.Define("eventMuon",getRankedMuonPt(rank),{"filteredMuons"}).Histo1D(model,"eventMuon").GetValue();
  
  if(total.find(rank) == total.end())
    total[rank] = filteredDataFrame.Define("eventMuon",getRankedMuonPt(rank),{"filteredMuons"}).Histo1D(model,"eventMuon").GetValue();
  
  return TriggerEfficiency(passedHistograms[rank],total[rank],triggerName,styleParams,fitParams);
}

ULong64_t TriggerEfficiencyInitializer::getCount()
{
  return *filteredCount;
}

ULong64_t TriggerEfficiencyInitializer::getCount(const std::string& triggerName)
{
  if(triggerMap.find(triggerName) == triggerMap.end())
    throw cms::Exception(triggerName+" Efficiency not found!");
  
  return triggerMap.find(triggerName)->second.filterCount.GetValue();
}

//Struct for passing in legend parameters
struct LegendParameters
{
  double x_min, y_min, x_max, y_max;
};

//Struct for passing in graph parameters
struct GraphParameters
{
  double x_min, x_max, y_min, y_max;
  bool deleteCanvas, noSpaces;
  std::vector<std::string> saveExtensions;
};
// -*- C++ -*-
//
// Package:    DPIO
// Class:      Muon
//
/*
 Description: Stores muons from a particle run.

 Implementation:
     [Notes on implementation]
*/
//
//          Original Author:  Ari Fishkin
//         Created:  Thu, 1 Apr 2021 15:52:13 CDT
//
// 
#ifndef DPIO_MUON_H
#include <bitset>
#include <vector>
#include "TObject.h"
#include "DataFormats/HepMCCandidate/interface/GenParticleFwd.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "TriggerCode/DPIO/interface/MuonSelector.h"
namespace DPIO
{
    enum class StatusFlag {
       kIsPrompt = 0,
       kIsDecayedLeptonHadron,
       kIsTauDecayProduct,
       kIsPromptTauDecayProduct,
       kIsDirectTauDecayProduct,
       kIsDirectPromptTauDecayProduct,
       kIsDirectHadronDecayProduct,
       kIsHardProcess,
       kFromHardProcess,
       kIsHardProcessTauDecayProduct,
       kIsDirectHardProcessTauDecayProduct,
       kFromHardProcessBeforeFSR,
       kIsFirstCopy,
       kIsLastCopy,
       kIsLastCopyBeforeFSR
     };
    class Muon : public TObject
    {
        private:
            Double_t pt;  //pT in GeV
            Double_t eta; //eta in radians
            Double_t phi; //phi in radians
            Double_t mass; //mass
            Double_t vx; //Vertex x in centimeters
            Double_t vy; //Vertex y in centimeters
            Double_t vz; //Vertex z in centimeters
            Int_t collisionId; //Collision id provided by gen particle
            Int_t statusCode; //Status code provided by gen particle
            std::bitset<15> statusFlags; //status flags stored in gen particle, for more information see the documentation for reco::GenParticle
            std::vector<Int_t> motherPdgIds; //pdgIds of mothers up the mother chain
            Muon(const reco::GenParticle& particle);
        public:
            Muon();
            Double_t getPt() const {return pt;}
            Double_t getEta() const {return eta;}
            Double_t getPhi() const {return phi;}
            Double_t getMass() const {return mass;}
            Double_t getVx() const {return vx;}
            Double_t getVy() const {return vy;}
            Double_t getVz() const {return vz;}
            Int_t getCollisionId() const {return collisionId;}
            const std::vector<Int_t>& getMotherPdgIds() const {return motherPdgIds;}
            bool getStatusFlag(StatusFlag statusFlag) const {return statusFlags[static_cast<int>(statusFlag)];}
            
            // Makes a vector of muons that can be stored in a DPIO::Event.
            static std::vector<Muon> makeFromMuonSelector(MuonSelector&& muonSelector);
ClassDef(Muon, 1)
    };
}
#define DPIO_MUON_H
#endif
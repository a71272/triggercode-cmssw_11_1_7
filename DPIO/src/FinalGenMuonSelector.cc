#include "TriggerCode/DPIO/interface/FinalGenMuonSelector.h" 
#include <cmath>
namespace DPIO
{
    const reco::GenParticle* DPIO::FinalGenMuonSelector::nextMuon()
    {
        reco::GenParticle* particle = collection->data() + position;
        while(position < collection->size())
        {
            ++position;
            const reco::GenParticle* particle = collection->data() + position - 1;
            if(std::abs(particle->pdgId()) == 13 && particle->isPromptFinalState())
            {
                return particle;
            }
        }
        return nullptr;
    }
}

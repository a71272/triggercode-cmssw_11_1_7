#include "TriggerCode/DPIO/interface/GenMuonSelector.h" 

namespace DPIO
{
    const reco::GenParticle* DPIO::GenMuonSelector::nextMuon()
    {
        while(position < collection->size())
        {
            ++position;
            const reco::GenParticle* particle = collection->data() + position - 1;
            if(std::abs(particle->pdgId()) == 13)
            {
                return particle;
            }
        }
        return nullptr;
    }
}
